﻿// See https://aka.ms/new-console-template for more information
using ArrayCRUD.Models;
using ArrayCRUD.Repository;

Console.WriteLine("Hello, World!");

ProductRepository prepo = new ProductRepository();
prepo.DisplayAllProducts();
Console.WriteLine("Which product do you want? (Plz enter name.)");
string searchName = Console.ReadLine().Trim();
Product[] pByName = prepo.GetProductsByName(searchName);
ProductRepository.DisplayProducts(pByName);
Console.WriteLine("\n");

prepo.DisplayAllProducts();
Console.WriteLine("Which product do you want? (Plz enter category.)");
string searchCat = Console.ReadLine().Trim();
Product[] pByCat = prepo.GetProductsByCategory(searchCat);
ProductRepository.DisplayProducts(pByCat);
Console.WriteLine("\n");

prepo.DisplayAllProducts();
Console.WriteLine("Which product do you want to delte? (Plz enter id.)");
int pid = int.Parse(Console.ReadLine().Trim());
prepo.DeleteProduct(pid);
Console.WriteLine("\n");

prepo.DisplayAllProducts();
Console.WriteLine("Which product do you want to update? (Plz enter id.)");
int pupid = int.Parse(Console.ReadLine().Trim());
Console.WriteLine("Enter new Name? (enter if want to keep same)");
string pname = Console.ReadLine().Trim();
Console.WriteLine("Enter new Category? (enter if want to keep same)");
string pcat = Console.ReadLine().Trim();
Console.WriteLine("Enter new Price? (enter if want to keep same)");
double pprice = double.Parse(Console.ReadLine().Trim());
prepo.UpdateProduct(pupid, pname, pcat, pprice);
prepo.DisplayAllProducts();
Console.WriteLine("\n");

