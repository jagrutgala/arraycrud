﻿using System;

namespace ArrayCRUD.Models {
    internal class Product {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Category { get; set; }
        public double Price { get; set; }

        public Product(int id, string name, string category, double price) {
            this.Id = id;
            this.Name = name;
            this.Category = category;
            this.Price = price;
        }

        public override string ToString() {
            return $"Id:{this.Id} Name:{this.Name} Category:{this.Category} Price:{this.Price}";
        }
    }
}
